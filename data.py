#!usr/bin/python3
# -*- coding: utf-8 -*-

"""
This script allows installing the data.
After installing the app with pip command, please run:

    $ python3 data.py install

    or

    $ python3 data.py uninstall

"""

import sys
import shutil
from pathlib import Path


class Data:

    def __init__(self):
        self.app: str = 'battery-notify'
        self.daemon: str = 'battery-daemon'
        self.config_path: Path = Path(f'/etc/{self.app}/')
        self.xdg_autostart_path: Path = Path('/etc/xdg/autostart/')
        self.pixmaps_path: Path = Path('/usr/share/pixmaps/')
        self.sounds_path: Path = Path(f'/usr/share/sounds/{self.app}/')
        self.config_dir: Path = Path(Path().absolute(), 'config/')
        self.data_dir: Path = Path(Path().absolute(), 'data/')
        self.xdg_dir: Path = Path(Path().absolute(), 'xdg/')

        self.config_file: str = 'config.toml'
        self.xdg_autostart_file: str = f'{self.daemon}.desktop.sample'
        self.pixmaps_file: str = f'{self.app}.png'
        self.sounds_file: str = f'{self.app}.mp3'
        self.install_paths: list = [self.config_path, self.xdg_autostart_path, self.pixmaps_path, self.sounds_path]

    def create_paths(self):
        for path in self.install_paths:
            if not path.is_dir():
                path.mkdir(parents=True, exist_ok=True)

    @staticmethod
    def yes_no():
        answer: str = input('\nDo you want to continue? [y/N] ')
        if answer not in ['Y', 'y']:
            raise SystemExit(0)

    def view_install(self):
        print('The following files will be installed:\n')
        print(f'{self.config_file} -> {self.config_path}/{self.config_file}')
        print(f'{self.xdg_autostart_file} -> {self.xdg_autostart_path}/{self.xdg_autostart_file}')
        print(f'{self.pixmaps_file} -> {self.pixmaps_path}/{self.pixmaps_file}')
        print(f'{self.sounds_file} -> {self.sounds_path}/{self.sounds_file}')

    def view_uninstall(self):
        count_uninstall: int = 0
        print('The following files will be uninstalled:\n')
        if Path(self.config_path, self.config_file).is_file():
            print(f'{self.config_path}/{self.config_file}')
            count_uninstall += 1
        if Path(self.xdg_autostart_path, self.xdg_autostart_file).is_file():
            print(f'{self.xdg_autostart_path}/{self.xdg_autostart_file}')
            count_uninstall += 1
        if Path(self.pixmaps_path, self.pixmaps_file).is_file():
            print(f'{self.pixmaps_path}/{self.pixmaps_file}')
            count_uninstall += 1
        if Path(self.sounds_path, self.sounds_file).is_file():
            print(f'{self.sounds_path}/{self.sounds_file}')
            count_uninstall += 1
        if count_uninstall == 0:
            raise SystemExit('Nothing to uninstall.')

    def install(self):
        self.view_install()
        self.yes_no()
        self.create_paths()
        shutil.copy(Path(self.config_dir, self.config_file),
                    Path(self.config_path, self.config_file))
        shutil.copy(Path(self.xdg_dir, self.xdg_autostart_file),
                    Path(self.xdg_autostart_path, self.xdg_autostart_file))
        shutil.copy(Path(self.data_dir, self.pixmaps_file),
                    Path(self.pixmaps_path, self.pixmaps_file))
        shutil.copy(Path(self.data_dir, self.sounds_file),
                    Path(self.sounds_path, self.sounds_file))
        print('\nInstall finished.')

    def uninstall(self):
        self.view_uninstall()
        self.yes_no()
        if Path(self.config_path).is_dir():
            shutil.rmtree(self.config_path)
        if Path(self.sounds_path).is_dir():
            shutil.rmtree(self.sounds_path)
        if Path(self.xdg_autostart_path, self.xdg_autostart_file).is_file():
            Path(self.xdg_autostart_path, self.xdg_autostart_file).unlink()
        if Path(self.pixmaps_path, self.pixmaps_file).is_file():
            Path(self.pixmaps_path, self.pixmaps_file).unlink()
        print('\nUninstall finished.')


def run():
    data = Data()
    args: list = sys.argv
    args.pop(0)
    if len(args) == 1 and 'install' == args[0]:
        data.install()
    elif len(args) == 1 and 'uninstall' == args[0]:
        data.uninstall()
    else:
        raise SystemExit('usage: battery-notify install|uninstall')


if __name__ == '__main__':
    run()
