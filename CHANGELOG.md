battery-notify
==============

Version 1.4 - *(19-02-2024)*
------------------------------
### Updated
- Fix autostart daemon desktop file

Version 1.3 - *(11-02-2024)*
------------------------------
### Updated
- Usage for battery daemon

Version 1.2 - *(10-02-2024)*
------------------------------
### Updated
- Build-system to flit_core buildsystem

Version 1.1 - *(10-02-2024)*
------------------------------
### Updated
- Readme file for install data
- Update data.py for usage
- Print messages for autostart enable and disable


- This is the first version
Version 1.0 - *(10-02-2024)*
------------------------------
### Released
- This is the first version
